const electron = require('electron')
const { app, BrowserWindow, Menu, session, shell } = require('electron')
var path = require('path')
const URL = require('url').URL

let mainWindow;

app.commandLine.appendSwitch('widevine-cdm-path', '/Applications/Google Chrome.app/Contents/Frameworks/Google Chrome Framework.framework/Versions/Current/Libraries/WidevineCdm/_platform_specific/mac_x64/')

// The version of plugin can be got from `chrome://components` page in Chrome.
app.commandLine.appendSwitch('widevine-cdm-version', '4.10.1440.18')

function createWindow() {
	mainWindow = new BrowserWindow({
		height: 1100,
		width: 1700,
		frame: true,
		icon: path.join(__dirname, 'assets/icons/png/64x64.png'),
		webPreferences: {
			backgroundThrottling: false,
			nodeIntegration: false,
			contextIsolation: true,
			preload: ''
		}
	});

	mainWindow.loadURL('https://app.napster.com');

	const mainMenu = Menu.buildFromTemplate(menuTemplate);
	Menu.setApplicationMenu(mainMenu);

	mainWindow.on('closed', () => {
		mainWindow = null
	})

	// Security
	session.fromPartition('some-partition')
			.setPermissionRequestHandler((webContents, permission, callback) => {
				const url = webContents.getURL()

				if (permission === 'notifications') {
					// Approves the permissions request
					callback(true)
				}

				// Verify URL
				if (!url.startsWith('https://app.napster.com/')) {
					// Denies the permissions request
					return callback(false)
				}
	})

	session.defaultSession
			.webRequest
			.onHeadersReceived((details, callback) => {
				callback({
					responseHeaders: {
						...details.responseHeaders,
						'Content-Security-Policy': ['script-src https://*.napster.com https://*.rhapsody.com https://*.lldns.net/ \'unsafe-inline\'; style-src https://*.napster.com \'unsafe-inline\'; font-src https://*.napster.com ']
					}
				})
	})
	
}


app.on('ready', createWindow);

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

app.on('activate', () => {
	if (mainWindow === null) {
		createWindow()
	}
})


// Menue
const menuTemplate = [
	{
		label: 'Napster',
		submenu: [
					{
						label: 'About Electron',
						selector: 'orderFrontStandardAboutPanel:'
					},
					{
						type: 'separator'
					},
					{
						label: 'Services',
						submenu: []
					},
					{
						type: 'separator'},
					{
						label: 'Hide Napster',
						accelerator: 'Command+H',
						selector: 'hide:'
					},
					{
						label: 'Hide Others',
						accelerator: 'Command+Shift+H',
						selector: 'hideOtherApplications:'
					},
					{
						label: 'Show All',
						selector: 'unhideAllApplications:'
					},
					{
						type: 'separator'
					},
					{
						label: 'Close',
						accelerator: 'Command+Q',
						click: function() { app.quit(); }
					},
		]
	},
	{
		label: 'Edit',
		submenu: [
					{
						label: 'Undo',
						accelerator: 'Command+Z',
						selector: 'undo:'
					},
					{
						label: 'Repeat',
						accelerator: 'Shift+Command+Z',
						selector: 'redo:'
					},
					{
						type: 'separator'
					},
					{
						label: 'Cut',
						accelerator: 'Command+X',
						selector: 'cut:'
					},
					{
						label: 'Copy',
						accelerator: 'Command+C',
						selector: 'copy:'
					},
					{
						label: 'Paste',
						accelerator: 'Command+V',
						selector: 'paste:'
					},
					{
						label: 'Select All',
						accelerator: 'Command+A',
						selector: 'selectAll:'
					},
		]
	},
	{
		label: 'View',
		submenu: [
					{
						label: 'Reload Window',
						accelerator: 'Command+R',
						click: function() { mainWindow.reload(); }
					},
					{
						label: 'Toggle Developer Tools',
						accelerator: 'Alt+Command+I',
						click: function() { mainWindow.toggleDevTools(); }
					},
		]
	},
	{
		label: 'Window',
		submenu: [
					{
						label: 'Minimize',
						accelerator: 'Command+M',
						selector: 'performMiniaturize:'
					},
					{
						label: 'Close',
						accelerator: 'Command+W',
						selector: 'performClose:'
					},
					{
						type: 'separator'
					},
					{
						label: 'Bring all to front',
						selector: 'arrangeInFront:'
					},
		]
	},
	{
		label: 'Help',
		submenu: []
	}
];



// Security
app.on('web-contents-created', (event, contents) => {
	contents.on('will-attach-webview', (event, webPreferences, params) => {
		// Strip away preload scripts if unused or verify their location is legitimate
		delete webPreferences.preload
		delete webPreferences.preloadURL

		// Disable Node.js integration
		webPreferences.nodeIntegration = false

		// Verify URL being loaded
		if (!params.src.startsWith('https://app.napster.com/')) {
			event.preventDefault()
		}
	})
})

app.on('web-contents-created', (event, contents) => {
	contents.on('will-navigate', (event, navigationUrl) => {
		const parsedUrl = new URL(navigationUrl)

		if (parsedUrl.origin !== 'https://app.napster.com') {
			event.preventDefault()
		}
	})
})

app.on('web-contents-created', (event, contents) => {
	contents.on('new-window', (event, navigationUrl) => {
		event.preventDefault()

		shell.openExternalSync(navigationUrl)
	})
})