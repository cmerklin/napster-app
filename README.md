# Napster App

An inofficial (macOS) Napster Application based on the Music Streaming Service [Napster](https://napster.com). The application only accepts loading content from Napster Server and blocks all Tracking like Google Analytics.

## Building the Application

```sh
# install dependencies
npm install

# build the application to ./dist
npm run pckage-mac

# build the installer to ./dist
npm run create-installer-mac
```

## License

MIT

[Napster](https://napster.com) and the [Napster logo](https://us.napster.com/style-guide) are registered trademarks of Rhapsody International Inc registered
in the United States and other countries.